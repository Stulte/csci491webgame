/**
 * Created by austinherron on 12/16/15.
 */
define(['Platform'], function(Platform){


    function Map(display){
        this.display = display;
        this.platforms = [];

        //hard code map in, add ability to load in map from JSON file

        //bounding box around canvas
        this.platforms.push(new Platform(0, display.height - 5, display.width, 5, "green"));
        this.platforms.push(new Platform(0, 0, 5, display.height, "green"));
        this.platforms.push(new Platform(0, 0, display.width, 5, "green"));
        this.platforms.push(new Platform(display.width - 5, 0, 5, display.height, "green"));

        //other platforms
        this.platforms.push(new Platform( display.width / 2 - 75, display.height / 2, 150, 10));
        this.platforms.push(new Platform( display.width / 4 - 75, display.height - display.height / 4, 150, 10));
        this.platforms.push(new Platform( display.width * 3 / 4 - 75, display.height - display.height / 4, 150, 10));

    }

    Map.prototype.addPlatform = function(platform){
        this.platforms.push(platform);
    };

    Map.prototype.getPlatforms = function(){
        return this.platforms;
    };

    Map.prototype.draw = function(ctx){
        ctx.beginPath();

        for(var i = 0; i < this.platforms.length; i++){
            ctx.fillStyle = this.platforms[i].color;
            ctx.fillRect(this.platforms[i].x, this.platforms[i].y, this.platforms[i].width, this.platforms[i].height);
            ctx.fillStyle = 'black';
        }

        ctx.closePath();
    };

    Map.CURRENT_MAP = this;

    return Map;
});