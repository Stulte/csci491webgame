#!/usr/bin/env node

var server = require('./../app');
var PORT = process.env.PORT || 8080;

server.listen(PORT, function(){
   console.log("Server listening on port " + PORT);
});