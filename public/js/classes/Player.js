define(['KeyboardHandler', 'CollisionHandler'], function(KeyboardHandler, CollisionHandler){

    var friction = .9;
    var gravity = .3;

    function Player(name, currentMap){

        this.name = name;
        this.x = 10;
        this.y = 10;
        this.height = 20;
        this.width = 20;
        this.speed = 3;
        this.velX = 0;
        this.velY = 0;
        this.grounded = false;
        this.jumping = false;
        this.currentMap = currentMap;


    }

    Player.prototype.update = function(deltaTime){
        if(KeyboardHandler.keys[KeyboardHandler.JUMP_KEY]){
            if(!this.jumping && this.grounded){
                this.jumping = true;
                this.grounded = false;
                this.velY = -this.speed * 3.5;
            }
        }
        if(KeyboardHandler.keys[KeyboardHandler.LEFT_KEY]){
            if(this.velX > -this.speed){
                this.velX--;
            }
        }
        if(KeyboardHandler.keys[KeyboardHandler.RIGHT_KEY]){
            if(this.velX < this.speed){
                this.velX++;
            }
        }

        this.velX *= friction;
        this.velY += gravity;

        this.grounded = false;

        //collision test!

        for(var i = 0; i < this.currentMap.platforms.length; i++){
            var dir = CollisionHandler.boundingBoxTest(this, this.currentMap.platforms[i]);

            if(dir === 'l' || dir === 'r'){
                this.velX = 0;
                this.jumping = false;
            }
            if(dir ==='b'){
                this.grounded = true;
                this.jumping = false;
            }
            if(dir === 't'){
                this.velY *= -1;
            }
        }

        if(this.grounded){
            this.velY = 0;
        }

        this.x += this.velX;
        this.y += this.velY;


    };

    Player.prototype.draw = function(ctx){
        ctx.fillStyle = "red";
        ctx.fillRect(this.x, this.y, this.width, this.height);

    };



    return Player;
});