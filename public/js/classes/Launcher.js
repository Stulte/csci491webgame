/**
 * Created by austinherron on 12/16/15.
 */
define(['Game', "Display", "KeyboardHandler"], function(Game, Display, KeyboardHandler){
    var display, game, keyboardHandler;

    function Launcher(width, height, parentNode){
        display = new Display(width, height, parentNode);
        keyboardHandler = new KeyboardHandler();
        bindKeyboardHandler();
        game = new Game(display, keyboardHandler);
        game.start();
    }

    function bindKeyboardHandler(){
        document.addEventListener('keydown', keyboardHandler.keydown.bind(keyboardHandler));
        document.addEventListener('keyup', keyboardHandler.keyup.bind(keyboardHandler));
    }

    return Launcher;
});