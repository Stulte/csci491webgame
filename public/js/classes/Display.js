/**
 * Created by austinherron on 12/16/15.
 */
define(function(){

    function Display(width, height, parentNode){
        this.width = width;
        this.height = height;
        this.createDisplay(parentNode);
    }

    Display.prototype.createDisplay = function(parentNode){
        parentNode.innerHTML = "<canvas id='gameCanvas' height='" + this.height + "' width='" + this.width + "'></canvas>";
    };

    Display.prototype.getContext = function(){
        return document.getElementById("gameCanvas").getContext('2d');
    };


    return Display;
});