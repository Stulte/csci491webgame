/**
 * Created by austinherron on 11/30/15.
 */
module.exports = function(io){
    var messages = [];
    var connectedUsers = [];

    io.on('connect', function(socket){
        for(var x = 0; x < messages.length; x++){
            socket.emit('message', messages[x]);
        }

        for(var x = 0; x < connectedUsers.length; x++){
            socket.emit('newUser', connectedUsers[x]);
        }

        socket.on('message', function(data){
            console.log("Message Received: \n" + data.username + ": " + data.message + "\n\n");
            socket.broadcast.emit('message', data);
            socket.emit('message', data);
            messages.push(data);
        });

        socket.on("newUser", function(username){
            socket.broadcast.emit('newUser', username);
            socket.emit('newUser', username);
            connectedUsers.push(username);
            socket.username = username;
        });

        socket.on('userExit', function(username){
            socket.broadcast.emit('userExit', username);
            socket.emit('userExit', username);
        });

        socket.on('disconnect', function(){
            userDisconnect();
        });

        function userDisconnect(){
            socket.broadcast.emit('userExit', socket.username);
            for(var x = 0; x < connectedUsers.length; x++){
                if(connectedUsers[x] === socket.username){
                    connectedUsers.splice(x, 1);
                }
            }
        }
    });
};