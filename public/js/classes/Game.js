/**
 * Created by austinherron on 12/16/15.
 */
define(['Player', 'Map'], function(Player, Map){
    //polyfill for window.requestAnimationFrame
    (function() {
        var lastTime = 0;
        var vendors = ['webkit', 'moz'];
        for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
            window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
            window.cancelAnimationFrame =
                window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
        }

        if (!window.requestAnimationFrame)
            window.requestAnimationFrame = function(callback, element) {
                var currTime = new Date().getTime();
                var timeToCall = Math.max(0, 16 - (currTime - lastTime));
                var id = window.setTimeout(function() { callback(currTime + timeToCall); },
                    timeToCall);
                lastTime = currTime + timeToCall;
                return id;
            };

        if (!window.cancelAnimationFrame)
            window.cancelAnimationFrame = function(id) {
                clearTimeout(id);
            };
    }());

    var running = false;
    var g, width, height;

    var player;

    var map;

    function run(){
        var fps = 60;
        var timePerUpdate = 1000/fps;
        var timer = 0;
        var delta = 0;
        var now;
        var lastTime = Date.now();

        function gameLoop(){
            if(running) {
                now = Date.now();
                delta = now - lastTime;
                timer += delta;

                if (timer >= timePerUpdate) {
                    update(timer/1000);
                    render();
                    timer = 0;
                }
                lastTime = Date.now();
                requestAnimationFrame(gameLoop)
            }
        }
        gameLoop();
    }

    function update(deltaTime){
        player.update(deltaTime);
    }

    function render(){
        g.clearRect(0, 0, width, height);
        map.draw(g);
        player.draw(g);

    }

    function Game(display){
        g = display.getContext();
        width = display.width;
        height = display.height;
        map = new Map(display);
        player = new Player("stulte", map);
    }

    Game.prototype.start = function(){
        if(running){
            return;
        }else{
            running = true;
            run();
        }
    };

    return Game;
});