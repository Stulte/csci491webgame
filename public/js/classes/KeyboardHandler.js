/**
 * Created by austinherron on 12/16/15.
 */
define(function(){

    function KeyboardHandler(){}

    KeyboardHandler.LEFT_KEY = 37;
    KeyboardHandler.RIGHT_KEY = 39;
    KeyboardHandler.JUMP_KEY = 32;
    KeyboardHandler.keys = [];

    KeyboardHandler.prototype.keydown = function(event){
        if(!KeyboardHandler.keys[event.keyCode]) {
            KeyboardHandler.keys[event.keyCode] = true;
        }
    };

    KeyboardHandler.prototype.keyup = function(event){
        if(KeyboardHandler.keys[event.keyCode]){
            KeyboardHandler.keys[event.keyCode] = false;
        }
    };

    return KeyboardHandler;
});