/**
 * Created by austinherron on 12/16/15.
 */
requirejs.config({
    baseUrl:"./js",
    paths:{
        Display: "classes/Display",
        Launcher: "classes/Launcher",
        Game: "classes/Game",
        KeyboardHandler: "classes/KeyboardHandler",
        Player: "classes/Player",
        CollisionHandler: "classes/CollisionHandler",
        Platform: "classes/Platform",
        Map: "classes/Map"
    }
});

require(["main"]);