/**
 * Created by austinherron on 11/30/15.
 */

var messageField = $("#messageInput");

$(function() {
    var socket = io.connect();
    var username = prompt("Enter your username");
    if(username === null || username === '' || /^\s+$/.test(username) || username.length > 16){
        alert("Invalid Username");
        username = "anon";
    }

    socket.emit('newUser', username);


    $("#sendMessageButton").on('click', function(){
        emitMessage({username: username, message: messageField.val()});
    });

    messageField.on('keydown', function(e){
        if(e.keyCode === 13) { //ENTER
            emitMessage({username: username, message: messageField.val()});
        }
    });

    socket.on('message', function(data){
        var messageDisplay = document.getElementById("messageDisplay");
        var message = document.createElement("p");
        message.appendChild(document.createTextNode(data.username + ": " + data.message));
        messageDisplay.appendChild(message);
        messageDisplay.scrollTop = messageDisplay.scrollHeight;
    });

    socket.on('newUser', function(username){
        var onlineUsers = document.getElementById("connectedUsersList");
        var newUserLi = document.createElement("li");
        newUserLi.appendChild(document.createTextNode(username));
        newUserLi.setAttribute("id", username);
        onlineUsers.appendChild(newUserLi);
    });

    socket.on('userExit', function(username){
        $('#' + username).remove();

    });

    socket.on('disconnect', function(){
        socket.emit('userExit', username);
    });

    function resetMessageField(){
        messageField.val("");
    }

    function emitMessage(data){
        socket.emit('message', data);
        resetMessageField();
    }
});



