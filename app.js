/**
 * Created by austinherron on 12/9/15.
 */

var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var chatServer = require("./ChatServer")(io);
var gameServer = require("./GameServer")(io);

app.use(express.static('public'));


module.exports = server;