/**
 * Created by austinherron on 12/9/15.
 */
var request = require("supertest");
var app = require("./app.js");

describe("Requests to /", function(){

   it("Returns 200 Status Code", function(done){
        request(app)
            .get('/')
            .expect(200, done);
   });
});