/**
 * Created by austinherron on 12/16/15.
 */
define(function(){
    function CollisionHandler(){}

    CollisionHandler.boundingBoxTest = function(rectA, rectB) {
        // get the vectors to check against
        var vX = (rectA.x + (rectA.width / 2)) - (rectB.x + (rectB.width / 2)),
            vY = (rectA.y + (rectA.height / 2)) - (rectB.y + (rectB.height / 2)),
        // add the half widths and half heights of the objects
            hWidths = (rectA.width / 2) + (rectB.width / 2),
            hHeights = (rectA.height / 2) + (rectB.height / 2),
            colDir = null;

        // if the x and y vector are less than the half width or half height, they we must be inside the object, causing a collision
        if (Math.abs(vX) < hWidths && Math.abs(vY) < hHeights) {         // figures out on which side we are colliding (top, bottom, left, or right)
            var oX = hWidths - Math.abs(vX),
                oY = hHeights - Math.abs(vY);
            if (oX >= oY) {
                if (vY > 0) {
                    colDir = "t";
                    rectA.y += oY;
                } else {
                    colDir = "b";
                    rectA.y -= oY;
                }
            } else {
                if (vX > 0) {
                    colDir = "l";
                    rectA.x += oX;
                } else {
                    colDir = "r";
                    rectA.x -= oX;
                }
            }
        }

        return colDir;
    };

    return CollisionHandler;
});