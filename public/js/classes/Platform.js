/**
 * Created by austinherron on 12/16/15.
 */
define(function(){
    function Platform(x, y, width, height, color){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.color = color || "black";
    }


    return Platform;
});